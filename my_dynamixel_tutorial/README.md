***********************************************************************************************************************************
****************************************//  my_dynamixel_tutorials //**************************************************************
The package contains the same tutorials as the online tutorial, but sepcifically customized for asl_husky.

Files:

1. controller_manager.launch -- Launch file to be launched first.
    Usage: roslaunch my_dynamixel_tutorial controller_manager.launch
    
2. start_meta_controller.launch -- Launch file for starting two dynamixel motors (i.e. Pan & tilt)
    Usage: roslaunch my_dynamixel_tutorial start_meta_controller.launch 
    
3. start_pan_controller.launch -- Launch filr for pan motor control (To be configured properly)
    Usage: roslaunch my_dynamixel_tutorial start_pan_controller.launch 
    
4. start_tilt_comntroller.launch -- Launch file for tilt motor control (to be configured properly)
    Usage: roslaunch my_dynamixel_tutorial start_tilt_controller.launch 
    
5. trajectory_client.py -- Python script to launch a trajectory described by angles of individual motors (!! Use with caution !!)
    Usage: python trajectory_client.py 
    
6. .yaml files -- Used as configuration files for each type of motors (pan,tilt, joint_trajectory_controller)
    Usage: To be edited as per the user requirement.
    

