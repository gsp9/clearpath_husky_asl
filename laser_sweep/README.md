**************************** Laser_sweep *************************************

Package cloned from repo : https://github.com/skasperski/laser_sweep.git

Dependancies:
loam_back_and_forth
(optional) dynamixel_motor


Modifications:
* Tried building a valid tf tree by changes made in file start.launch
mti/data :tf of imu
base_link :tf of husky base
camera :tf from loam_back_and_forth


Command Sequence:

roslaunch laser_sweep start.launch
roslaunch laser_sweep loam_back_and_forth.launch


!! Issues: No transform linked with the base & rotating lidar. Need a transform broadcaster (dynamic) which relates to the base and the roatting lidar as hinted in the file 'sweep_node.cpp' !!
!! Other files like driver.launch & controller.launch specifically for integrating dynamixel node with the laser_sweep package.!!


// For package usage & other issues please contact: gaurav.pandit2793@gmail.com //
