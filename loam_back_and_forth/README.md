***** LOAM_BACK_AND_FORTH ****
Standard package as cloned form the git repo : https://github.com/dani-carbonell/loam_back_and_forth.git 


Changes:
In file loam_back_and_forth.launch
(* Added Scan2PCL node to launch file from package 'laser_sweep' to convert laser_scan to PointCloud *

?? Loam back and forth subscibes to PointCloud messages as '/sync_scan_cloud_filtered', which is needed to be made available by the use of Scan2PCL node ??

!! Caution: The node renders unstable behaviour as all the launch files are run which causes the scanRegistration node to fail & die prematurely. Solution needed to be found. (I guess there's a problem with the tf tree while running all nodes. !!)

In file scanRegistration.cpp
( * for the imu messages, since we are using the xsens mti_sensor, the subscribed topic is changed to '/mti/sensor/imu' instead of the default topic*)

New File: trnsfm.launch
publishes a static transform from /camera to /camera_init_2.

Command Sequence:

start up the urg_node with the static ip 192.168.0.10 ( do a reverse search: urg_node on huskya200 system for exact command)

roslaunch loam_back_and_forth loam_back_and_forth.launch

set up static transform: roslaunch trnsfm.launch




// For any support related to the package & its usage please contact me at : gaurav.pandit2793@gmail.com //
